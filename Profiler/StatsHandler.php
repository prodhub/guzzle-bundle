<?php

namespace ADW\GuzzleBundle\Profiler;

use GuzzleHttp\TransferStats;

/**
 * Class StatsHandler
 *
 * @author Artur Vesker
 */
class StatsHandler
{

    /**
     * @var TransferStats[]
     */
    protected $stats = [];

    /**
     * @param TransferStats $stats
     */
    public function __invoke(TransferStats $stats)
    {
        $this->stats[] = $stats;
    }

    /**
     * @return \GuzzleHttp\TransferStats[]
     */
    public function getStats()
    {
        return $this->stats;
    }

}
