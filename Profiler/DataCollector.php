<?php

namespace ADW\GuzzleBundle\Profiler;

use Symfony\Component\HttpKernel\DataCollector\DataCollector as BaseCollector;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DataCollector.
 *
 * @author Artur Vesker
 */
class DataCollector extends BaseCollector
{
    /**
     * @var array
     */
    protected $data;

    /**
     * @var StatsHandler
     */
    protected $statsHandler;

    /**
     * @param StatsHandler $statsHandler
     */
    public function __construct(StatsHandler $statsHandler)
    {
        $this->statsHandler = $statsHandler;
    }

    /**
     * @param Request    $request
     * @param Response   $response
     * @param \Exception $exception
     */
    public function collect(Request $request, Response $response, \Exception $exception = null)
    {
        $this->data['calls'] = [];

        foreach ($this->statsHandler->getStats() as $stats) {
            $call['request_body'] = (string) $stats->getRequest()->getBody();
            $call['response_body'] = $stats->hasResponse() ? (string) $stats->getResponse()->getBody() : null;
            $call['request'] = $stats->getRequest();
            $call['response'] = $stats->getResponse();
            $call['duration'] = $stats->getTransferTime();
            $this->data['calls'][] = $call;
        }
    }

    /**
     * @return array
     */
    public function getCalls()
    {
        return $this->data['calls'];
    }

    /**
     * @return int
     */
    public function getCallsCount()
    {
        return count($this->data['calls']);
    }

    /**
     * @return int
     */
    public function totalTime()
    {
        $time = 0;

        if ($this->data['calls']) {
            foreach ($this->data['calls'] as $call) {
                $time += $call['duration'];
            }
        }

        return $time;
    }

    /**
     * @return int
     */
    public function countErrors()
    {
        $count = 0;

        if ($this->data['calls']) {
            foreach ($this->data['calls'] as $call) {
                if ($call['response']->getStatusCode() > 399) {
                    ++$count;
                }
            }
        }

        return $count;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'adw_guzzle';
    }

    public function reset()
    {
        $this->data = [];
    }


}
