<?php

namespace ADW\GuzzleBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class ADWGuzzleExtension extends Extension
{

    use GuzzleDependencyInjectionTrait;

    /**
     * @inheritdoc
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('adw_guzzle.config', $config);

        foreach ($config['clients'] as $name => $config) {

            $logger = null;

            if ($config['logger']['enabled']) {
                $logger = $this->buildLogger(
                    $container,
                    $name,
                    $config['logger']['channel'],
                    $config['logger']['format'],
                    $config['logger']['formatter']
                );
            }

            $this->buildClient(
                $container,
                $name,
                $config['handler']['id'],
                $config['handler']['stack'],
                $config['options'],
                $config['profiler'],
                $logger
            );
        }

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }

}
