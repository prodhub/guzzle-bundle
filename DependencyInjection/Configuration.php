<?php

namespace ADW\GuzzleBundle\DependencyInjection;

use GuzzleHttp\MessageFormatter;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('adw_guzzle');

        $rootNode
            ->children()
                ->arrayNode('clients')
                    ->useAttributeAsKey('name')
                    ->prototype('array')
                        ->addDefaultsIfNotSet()
                        ->children()
                            ->arrayNode('handler')
                                ->addDefaultsIfNotSet()
                                ->children()
                                    ->scalarNode('id')->defaultValue(null)->end()
                                    ->arrayNode('stack')
                                        ->prototype('scalar')->end()
                                    ->end()
                                ->end()
                            ->end()
                            ->variableNode('options')->defaultValue([])->end()
                            ->arrayNode('logger')
                                ->addDefaultsIfNotSet()
                                ->children()
                                    ->booleanNode('enabled')->defaultTrue()->end()
                                    ->scalarNode('channel')->defaultValue('guzzle')->end()
                                    ->scalarNode('format')->defaultValue(MessageFormatter::CLF)->end()
                                    ->scalarNode('formatter')->defaultNull()->end()
                                ->end()
                            ->end()
                            ->booleanNode('profiler')->defaultTrue()->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
