<?php

namespace ADW\GuzzleBundle\DependencyInjection;

use GuzzleHttp\MessageFormatter;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class GuzzleDependencyInjectionTrait
 *
 * @author Artur Vesker
 */
trait GuzzleDependencyInjectionTrait
{

    /**
     * @param ContainerBuilder $container
     * @param $key
     * @param null $baseHandler
     * @param array $handlers
     * @return string
     */
    protected function buildHandlerStack(ContainerBuilder $container, $key, $baseHandler = null, array $handlers = [])
    {
        $handlerStackDefinition = new Definition('GuzzleHttp\HandlerStack', [$baseHandler]);
        $handlerStackDefinition->setFactory(['GuzzleHttp\HandlerStack', 'create']);

        foreach ($handlers as $name => $handler) {
            $handlerStackDefinition->addMethodCall('push', [new Reference($handler), is_string($name) ? $name : null]);
        }

        $handlerStackId = 'adw_guzzle.handler_stack.'.$key;
        $container->setDefinition($handlerStackId, $handlerStackDefinition);

        return $handlerStackId;
    }

    /**
     * @param ContainerBuilder $container
     * @param $key
     * @param string $channel
     * @param string $format
     * @param null $formatter
     * @return string
     */
    protected function buildLogger(ContainerBuilder $container, $key, $channel = 'guzzle', $format = MessageFormatter::CLF, $formatter = null)
    {
        if (!$formatter) {
            $formatter = 'adw_guzzle.' . $key . '.logger.message_formatter';
            $container->setDefinition($formatter, new Definition('GuzzleHttp\MessageFormatter', [$format]));
        }

        $loggerId = 'adw_guzzle.' . $key . '.logger';

        $loggerDefinition = new Definition('\Closure', [
            new Reference('logger'),
            new Reference($formatter)
        ]);

        $loggerDefinition->setFactory(['GuzzleHttp\Middleware', 'log']);
        $loggerDefinition->addTag('monolog.logger', ['channel' => $channel]);

        $container->setDefinition($loggerId, $loggerDefinition);

        return $loggerId;
    }

    /**
     * @inheritdoc
     */
    protected function buildClient(ContainerBuilder $container, $name, $baseHandler = null, array $stack = [], array $options = [], $enableProfiler = true, $logger = null)
    {
        $definition = new Definition('GuzzleHttp\Client');

        if ($enableProfiler) {
            $options['on_stats'] = new Reference('adw_guzzle.profiler_stats_handler');
        }

        $handlerId = $baseHandler;

        if (count($stack) > 0 || !$baseHandler) {

            if ($logger) {
                $stack[] = $logger;
            }

            $handlerId = $this->buildHandlerStack($container, $name, $baseHandler, $stack);
        }

        $definition->addArgument(array_merge(['handler' => new Reference($handlerId)], $options));

        $name = 'guzzle.' . $name;

        $container->setDefinition($name, $definition);

        return $name;
    }

}
