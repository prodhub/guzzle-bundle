# GuzzleBundle

## Installation
```
#!bash
composer require adw/guzzle-bundle ~0.2

```
##### AppKernel.php
```
public function registerBundles()
{
  $bundles = [
    ...
    new ADW\GuzzleBundle\ADWGuzzleBundle(),
  ]
}
```

##### composer.json
```
#!json
"repositories": [
   { "type": "vcs", "url": "https://bitbucket.org/prodhub/guzzle-bundle.git" }
]
```

## Configuration

```
adw_guzzle:
      clients:
            my_client: ~
```

## Usage
```
$myClient = $container->get('guzzle.my_client');
```

## Run tests

Add to **app/phpunit.xml** code:

    <listeners>
        <listener 
            class="PHPUnit_Util_Log_VCR" 
            file="../vendor/php-vcr/phpunit-testlistener-vcr/PHPUnit/Util/Log/VCR.php" />
    </listeners>

Run test with command:

    $ phpunit -c app/
